#!/bin/bash

# files etension to check
FILE_TYPES="xml impex js java less css"

# set -f # avoid star (*) to be expanded

# for i in $FILE_TYPES; do
#     if [[ $ARGS != "" ]]; then
#         ARGS="$ARGS -or"
#     fi
#     ARGS="$ARGS -name *.$i"
# done

# # file version (very slow)
# #find . $ARGS -exec file "{}" ";" | grep CRLF | cut -f 1 -d ':'

# # grp version
# cr="$(printf "\r")"
# echo $ARGS
# echo 'find . $ARGS -exec grep -Ilsr "${cr}$" "{}" ";"'
# find . $ARGS -exec grep -Ilsr "${cr}$" "{}" ";"

# set +f





set -f # avoid star (*) to be expanded

for i in $FILE_TYPES; do
    if [[ $ARGS != "" ]]; then
        ARGS="$ARGS\|"
    fi
    ARGS="$ARGS$i"
done

# file version (very slow)
#find . -regex ".*\.\($ARGS\)" -exec file "{}" ";" | grep CRLF | cut -f 1 -d ':'

# grep version
cr="$(printf "\r")"
find . -regex ".*\.\($ARGS\)" -exec grep -Ilsr "${cr}$" "{}" ";"

set +f



