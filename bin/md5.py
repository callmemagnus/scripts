#!/usr/bin/env python

import hashlib
import sys

args = sys.argv

if ( len(args) == 1 ):
  print "missing text to md5"
  exit(1)

for i in range(len(args)-1):
  print hashlib.md5(args[i+1]).hexdigest(), args[i+1]

