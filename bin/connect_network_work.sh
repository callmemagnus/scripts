#!/bin/bash

DEFAULT_REMOTE=10.240.21.31

export http_proxy="http://localhost:3128"
export https_proxy="https://localhost:3128"

if [[ -n $1 ]]
then
	REMOTE_HOST=$1
else
	REMOTE_HOST=$DEFAULT_REMOTE
fi



ssh ${REMOTE_HOST} -L24800:localhost:24800 -L3128:localhost:3128 -X
