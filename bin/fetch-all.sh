#!/bin/bash

function git_fetch() {
    $(cd $i;git fetch);
}

function svn_update() {
    echo "svn repo here";
}

function update_repo() {
    echo -e "\e[4m$1\e[24m";
    if [ -d "$i/.git" ]; then
        git_fetch $i;

    elif [ -d "$i.svn" ]; then
        svn_update $i;
    fi
    echo "";
}


echo -e "let's add your \e[1mssh key\e[21m to avoid typing it multiple time"
ssh-add

for i in `ls`;
do
    if [ -d $i ];
    then
        update_repo $i;
    fi
done;
