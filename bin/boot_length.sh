#!/bin/bash

for file in /var/log/dmesg*;do
     printf "%-12s %12.6f\n" ${file##*/} $(
        ( [ "${file##*.}" == "gz" ] && zcat $file || cat $file) |
         sed -ne '$s/^\[ *\([0-9.]*\)\].*$/\1/p')
     done
