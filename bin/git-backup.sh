#!/bin/bash


# */5 07-18 * * 1-5 /home/magnus/bin/git-backup.sh /home/magnus/src/nc2/nesclub2_trunk /home/magnus/src/backups/nesclub2_trunk/
# */5 07-18 * * 1-5 /home/magnus/bin/git-backup.sh /home/magnus/src/nc2/nesclub2_4.3 /home/magnus/src/backups/nesclub2_4.3/


function usage() {
    echo "Usage ${0} <git repo> <backup directory>"
    echo $1
    exit
}

RELATIVE_DIR="" #`pwd`

if [ "$1" == "" ]; then
    usage "missing git dir"
fi

if [ "$2" == "" ]; then
    usage "missing backup dir"
fi

GIT_DIR=$1
BACKUP_DIR=$2

if [ ! -d "$GIT_DIR" ]; then
    usage "git_repo : not a directory"
fi
if [ ! -d "$BACKUP_DIR" ]; then
    echo "creating the backup directory"
    mkdir -p $RELATIVE_DIR/$BACKUP_DIR
fi

cd $GIT_DIR

git status > /dev/null

if [[ "$?" != "0" ]]; then
    usage "not a git repository"
fi

FILES=`git status --short | cut -c 4-300`
DATE=`date +%Y%m%d%H%M`

mkdir $RELATIVE_DIR/$BACKUP_DIR/$DATE

echo "Backing up ${GIT_DIR} in ${BACKUP_DIR}"

for i in $FILES; do

    DIR=`dirname $i`

    if [[ "$DIR" != "" ]]; then
        echo "[-] mkdir -p $RELATIVE_DIR/$BACKUP_DIR/$DATE/$DIR"
        mkdir -p $RELATIVE_DIR/$BACKUP_DIR/$DATE/$DIR
    fi

    if [[ -d $i ]]; then
        # is a directory
        #statements
        echo "[d] cp -r $i $RELATIVE_DIR/$BACKUP_DIR/$DATE/$DIR"
        cp -r $i $RELATIVE_DIR/$BACKUP_DIR/$DATE/$DIR
    else

        #local FILE=`basename $i`
        echo "[f] cp $i $RELATIVE_DIR/$BACKUP_DIR/$DATE/$DIR"
        cp $i $RELATIVE_DIR/$BACKUP_DIR/$DATE/$DIR
    fi

done