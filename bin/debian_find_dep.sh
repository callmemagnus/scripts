#!/bin/bash

PKGS=`dpkg --get-selections | egrep "install$" | cut -f 1`

# echo $PKGS

for pkg in $PKGS; do
    a=''
    a=`aptitude why $pkg | grep "^Unable"`
    if [[ $a != '' ]]; then
        echo $pkg
    fi
done
