#!/bin/bash

# reference: http://www.mikerubel.org/computers/rsync_snapshots/

OLDWD=$PWD
MAX_INCREMENTAL_TO_KEEP=9

if [ $# -eq 0 ]; then
    echo "No arguments supplied"
    exit
fi

if [ $# -eq 1 ]; then
    SOURCE=$1
    TARGET=/tmp
fi

if [ $# -eq 2 ]; then
    SOURCE=$1
    TARGET=$2
fi

if [ ! -d $SOURCE ]; then
    echo "source \"$SOURCE\" does not exist"
    exit
fi

if [ ! -d $TARGET ]; then
    echo -n "target \"$TARGET\" does not exist... "
    mkdir $TARGET
    echo "created"
fi

cd $TARGET

# removing oldest
if [[ -d backup.$MAX_INCREMENTAL_TO_KEEP ]]; then
    echo "deleting - $SOURCE - backup.$MAX_INCREMENTAL_TO_KEEP"
    rm -rf backup.$MAX_INCREMENTAL_TO_KEEP
fi

# moving intermediate
# mv n -> n+1
for i in $(seq $((MAX_INCREMENTAL_TO_KEEP-1)) -1 1); do
    if [[ -d backup.$i ]]; then
        echo "moving - $SOURCE - backup.$i backup.$((i+1))"
        mv backup.$i backup.$((i+1))
    fi
done

# copying 0 > 1
if [[ -d backup.0 ]]; then
    echo "copying - $SOURCE - backup.0 backup.1"
    cp -al backup.0 backup.1
fi

if [[ ! -d $SOURCE ]]; then
    # $SOURCE is not absolute
    SOURCE=$OLDWD/$SOURCE
fi

if [[ ! -d backup.0 ]]; then
    mkdir backup.0
fi

echo "rsyncing - $SOURCE on backup.0"

rsync -a -P -h --delete --exclude node_modules --exclude target/ --exclude '*.class' --exclude generated/ $SOURCE/  backup.0
touch backup.0
