#!/bin/bash

EXCLUDES="node_modules excluded"
RSYNC_OPTIONS="-av --delete"
MOUNT=/media/$USER


CURRENT=$(pwd)

err() {
  echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $@" >&2
}



rsync_to() {
  echo -e "\n\e[4mSummary of what will happen:\e[24m"
  printf "\e[1m%-15s\e[21m : %s\n" "Source" $1
  printf "\e[1m%-15s\e[21m : %s\n" "Target" $2
  printf "\e[1m%-15s\e[21m : %s\n" "Exclusions" "$EXCLUDES"
  printf "\e[1m%-15s\e[21m : %s\n" "Rsync options" "$RSYNC_OPTIONS"

  echo -e "\n"
  rsync $RSYNC_OPTIONS --exclude ${EXCLUDES// / --exclude } $1 $2
}

if [[ ! -d /$MOUNT ]]; then
  err "no media connected on $MOUNT"
  exit 1
fi

# has multiple directories?
MEDIA=$(ls --format=single-column $MOUNT)

if [[ $(echo "$MEDIA" | wc -l) > 1   ]]; then
  # multiple
  echo -e "\nThere are multiple devices attached on this computer:\n"
  PS3='Select target device: '
  select opt in $MEDIA
  do
    echo "Selected: $opt"
    TARGET=$MOUNT/$opt
    break
  done
else
  echo 'one'
  # only one
  TARGET=$MOUNT/$MEDIA
fi

rsync_to $CURRENT $TARGET
