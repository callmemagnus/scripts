#!/usr/bin/env bash

GIT_DIR=`git rev-parse --show-toplevel || echo "."`

J_HOME=/usr/lib/jvm/java-7-oracle

PLATFORM="$GIT_DIR/hybris/bin/platform"
EXEC_MVN=${MVN:-"$HOME/tools/apache-maven-3.2.3/bin/mvn"}
MVN_OPTIONS="-Xmx4072m -Xms256m -XX:+UseConcMarkSweepGC -XX:MaxPermSize=4048m"
ENV_OPTIONS="-Denv.ANT_AUDIT_DISABLED=true"
export JAVA_HOME=$J_HOME
export ANT_OPT="--Xmx5g -Xms256m -XX:+UseConcMarkSweepGC -XX:MaxPermSize=5g"
EXEC_HYBRIS="./hybrisserver.sh"
EXEC_ANT="ant"

# hybris data switch directories
DATA_DIR=$HOME/src/datas

cd $GIT_DIR || exit 1
ls hybris > /dev/null || exit 1

cd $PLATFORM
. ./setantenv.sh > /dev/null
export -p ANT_OPTS="-Xmx5g -Xms256m -XX:+UseConcMarkSweepGC -XX:MaxPermSize=5g"

function title() {
    echo -e "\e[1m$1\e[0m"
}

function usage() {
    echo "I do nothing without one of the following argument:"

    echo -e "
- \e[1mconsole\e[0m - starts hybris in the foreground
- \e[1mdebug\e[0m - starts hybris in the foreground (debug mode)
- \e[1mstart\e[0m - starts hybris in the background
- \e[1mstop\e[0m - stops hybris
- \e[1mrestart\e[0m - stops/starts hybris
- \e[1minit\e[0m - performs the initialise task
- \e[1mupdatesystem\e[0m - performs updatesystem task
- \e[1mfullupdatesystem\e[0m - performs before/updatesystem/after task
- \e[1mrecompile\e[0m - stops/recompile and eventually starts hybris
- \e[1mcodeupdate\e[0m - recompile with code update
- \e[1mimport_production\e[0m - launch import database with production dump
- \e[1mgrunt\e[0m - find directories containing Gruntfile.js
- \e[1msonar\e[0m - launch sonar
"
    exit
}

# function setantenv() {
#   cd $DIR_NC2/hybris/bin/platform
#   PLATFORM_HOME=`pwd`
#   ANT_OPTS=-Xmx200m
#   ANT_HOME=$PLATFORM_HOME/apache-ant-1.8.2
#   chmod +x "$ANT_HOME/bin/ant"
#   PATH=$ANT_HOME/bin:$PATH
#   ant $1 >> $FILE_LOG || wrap_up 1
# }

# set temp directory to match hybris
export CATALINA_TMPDIR=$GIT_DIR/hybris/temp/hybris

case $1 in
    "ant")
        $EXEC_ANT -version -diagnostics
        ;;
    "mvn")
        $EXEC_MVN --version
        ;;
    "clean" )
        $0 stop
        cd $GIT_DIR
        JAVA_HOME=$J_HOME $EXEC_MVN clean
        ;; 
    "start" )
        $0 stop
        JAVA_HOME=$J_HOME $EXEC_HYBRIS start
        ;;
    "debug" )
        $0 stop
        JAVA_HOME=$J_HOME $EXEC_HYBRIS debug
        ;;
    "fulltest" )
        $0 stop
        cd $GIT_DIR
        JAVA_HOME=$J_HOME $EXEC_MVN test -Denv.MAVENT_OPTS="$MVN_OPTIONS"
        ;;
    "test" )
        $0 stop
        cd $GIT_DIR
        JAVA_HOME=$J_HOME $EXEC_MVN test $ENV_OPTIONS -Denv.MAVENT_OPTS="$MVN_OPTIONS"
        ;;
    "stop" )
        JAVA_HOME=$J_HOME $EXEC_HYBRIS stop
        ;;
    "console" )
        $0 stop
        JAVA_HOME=$J_HOME $EXEC_HYBRIS
        ;;
    "updatesystem" )
        title "Updatesystem..."
        $0 stop
        ant updatesystem -Dtenant=master || exit 1
        ;;
    "fullupdatesystem" )
        title "Updatesystem..."
        $0 stop
        ant -f support.xml executepatch -Dtenant=master -Dphase=before_update || exit 1
        ant updatesystem -Dtenant=master || exit 1
        ant -f support.xml executepatch -Dtenant=master -Dphase=after_update || exit 1
        ant -f support.xml cleanuptypesystem -Dtenant=master -DremoveTypes=yes -DremoveInstances=yes || exit 1
        ;;
    "restart" )
        $0 stop || exit 1
        $0 start
        ;;
    "init" )
        ant initialize -Dtenant=master || exit 1
        ;;
    "recompile" )
        $0 stop
        cd $GIT_DIR
        JAVA_HOME=$J_HOME $EXEC_MVN compile $ENV_OPTIONS -Denv.MAVEN_OPTS="$MVN_OPTIONS" || exit 1
        ;;
    "codeupdate" )
        $0 stop
        g.safe_update.sh || exit 1
        cd $GIT_DIR
        JAVA_HOME=$J_HOME $EXEC_MVN clean || exit 1
        JAVA_HOME=$J_HOME $EXEC_MVN compile $ENV_OPTIONS -Denv.MAVEN_OPTS="$MVN_OPTIONS" || exit 1
        ;;
    "cleanup" )
        title "cleanup"
        $0 stop
        JAVA_HOME=$J_HOME $EXEC_MVN clean
        rm -rf $CATALINA_TMPDIR
        ;;
    "grunt" )
        title "grunt found in this project"
        find $GIT_DIR -type f -name Gruntfile.js -exec dirname {} \; | grep -v node_modules
        ;;
    "sonar" )
        title "sonar..."
        cd ../custom
        ant -f build-sonar.xml sonarpreview
        ;;

    "create_data_links" )
        title "linking from $DATA_DIR"
        echo "Note that this does not create the usable data link."
        cd $GIT_DIR/hybris
        for i in `ls $DATA_DIR`; do
            ls $i 2>&1 > /dev/null && continue
            ln -s $DATA_DIR/$i
            echo "link $i added"
        done
        ;;
    "import_production" )
        title "importing production database"

        echo "This will take a lot of time, are you sure ? (Type \"yes\" or \"y\")"
        read YES

        if [[ $YES == "yes" || $YES == "y" ]]; then
            cd $PLATFORM
            rm -rf ../../data
            $EXEC_ANT -f support.xml update-credentials-for-import-prod-database || (echo "update-credentials-for-import-prod-database failed" && exit 1)
            $EXEC_ANT -f support.xml import-prod-database || (echo "import-prod-database failed" && exit 1)
            mv ../../data/hsqldb-imported ../../data/hsqldb
            rm -r ../../data/media/sys_master
            mkdir -p ../../data/media/sys_master
            $EXEC_ANT -f support.xml import-prod-media || (echo "import-prod-media failed" && exit 1)
            $EXEC_ANT -f support.xml executepatch -Dtenant=master -Dphase=before_update || (echo "before_update failed" && exit 1)
            $EXEC_ANT updatesystem -Dtenant=master || (echo "updatesystem failed" && exit 1)
            $EXEC_ANT -f support.xml executepatch -Dtenant=master -Dphase=after_update ||( echo "after_update failed" && exit 1)
            $EXEC_ANT -f support.xml cleanuptypesystem -Dtenant=master -DremoveTypes=yes -DremoveInstances=yes || (echo "cleanuptypesystem failed" && exit 1)
        fi

        ;;
    * )
        usage
        ;;
esac

if [[ $2 != "" ]]; then
    $0 $2
fi
