#!/bin/bash
# taken from http://www.akadia.com/services/ssh_test_certificate.html


# generate private key
echo "Generating private key"
openssl genrsa -des3 -out server.key 1024

# sign request
echo "Generating sign request"
openssl req -new -key server.key -out server.csr

# remove passphrase
echo "Removing passphrase"
openssl rsa -in server.key -out server.key 

# self sign it
echo "Self-signing"
openssl x509 -req -days 3650 -in server.csr -signkey server.key -out server.crt

echo "Done"
