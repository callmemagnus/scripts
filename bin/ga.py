#!/usr/bin/env /usr/bin/python
# google auth
# from http://stackoverflow.com/questions/8529265/google-authenticator-implementation-in-python

import hmac, base64, struct, hashlib, time

def get_hotp_token(secret, intervals_no):
	key = base64.b32decode(secret, True)
	msg = struct.pack(">Q", intervals_no)
	h = hmac.new(key, msg, hashlib.sha1).digest()
	o = ord(h[19]) & 15
	h = (struct.unpack(">I", h[o:o+4])[0] & 0x7fffffff) % 1000000
	return h

def get_totp_token(secret):
	return get_hotp_token(secret, intervals_no=int(time.time())//30)

def main():
	# get secret from cli
	secret = raw_input("What's the secret ? ")
	for i in xrange(1, 10):
		print "{:3d} {:06d}".format(i, get_hotp_token(secret, intervals_no=i))

	print "Current token: {}".format(get_totp_token(secret))

if __name__ == '__main__':
	main()

