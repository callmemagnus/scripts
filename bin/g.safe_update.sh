#!/bin/bash

CONFIG_FILE=".git_safeupdate"
# config file currently supports the following commands variables
# git_safeupdate_checkout_before="xxx/yyy zzz/aaa"
# -> will checkout those files/directory prior rebasing
#
# git_safeupdate_kill_before="node.*grunt\ watch[h] git"
# -> will search for those processes and kill them


GIT_ROOT=`git rev-parse --show-toplevel`
OLPWD=`pwd`

cd $GIT_ROOT

if [[ -r ./$CONFIG_FILE ]]; then
    source ./$CONFIG_FILE

    if [[ $git_safeupdate_kill_before != '' ]]; then
        for i in $git_safeupdate_kill_before; do
            pgrep -f $i > /dev/null && pkill -f $i
        done
    fi

    if [[ $git_safeupdate_checkout_before != '' ]]; then
        for i in $git_safeupdate_checkout_before; do
            git checkout -- $i
        done
    fi
fi

git stash save "automatic safe pull rebase"
git pull --rebase
#git whatchanged --oneline

#git stash list

cd $OLPWD