#!/bin/bash

echo -e "\e[4mSwitching CNTLM NoProxy config\e[24m";

echo -e "\nAdministration \e[1mpassword\e[21m may be required"
echo ""

FILE=/etc/cntlm.conf

sudo echo -e "\e[1mFrom:\e[21m"
sudo grep "^NoProxy" $FILE
echo ""

# Switch
sudo sed "s/NoProxy/#&/" -i $FILE
sudo sed "s/^##NoProxy/NoProxy/" -i $FILE

echo -e "\e[1mTo:\e[21m"
sudo grep "^NoProxy" $FILE
echo ""


sudo service cntlm restart
