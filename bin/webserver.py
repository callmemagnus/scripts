#!/usr/bin/env python
"""
Simple web server v1.0

better than SimpleHTTPServer because:
- shows file size, and modification date

- enable to export current directory as zip
Start it the folder you want to expose
<script name> [<port>]

"""

import datetime, socket, array, struct, fcntl
import cgi, os, SocketServer, sys, time, urllib, signal, zipfile, urlparse
from os import curdir, sep
from SimpleHTTPServer import SimpleHTTPRequestHandler
from StringIO import StringIO
import sys, getopt
import re


def usage():
	print '''Usage: %s [-p|--port <port>] [-t|--diein <time in minutes>]

Options:

- -p or --port <int> :

if not specified server will start in any available port

- -t or --diein <int>:

time in minutes after which the server will not serve a new query (default: 3 min)

''' % sys.argv[0]
	sys.exit(0)


def cleanup(*args):
	global httpd
	print "\nshutting down"
	#httpd.shutdown()
	exit()

def main(argv):
	global httpd, die_at

	port = 0
	die_in = 3

	# clean finish
	signal.signal(signal.SIGINT, cleanup)
	signal.signal(signal.SIGTERM, cleanup)

	# get arguments
	try:
		opts, args = getopt.getopt(argv,"hp:t:",["port=","diein="])
	except getopt.GetoptError:
		usage()

	for opt, arg in opts:
		if opt == '-h':
			usage()
		elif opt in ('-p', '--port'):
			port = int(arg)
		elif opt in ('-t', '--diein'):
			die_in = int(arg)

	# calculate the die time
	starttime = datetime.datetime.now()
	die_at = starttime + datetime.timedelta(minutes=die_in)


	# start server
	try:
		httpd = StoppableHttpServer(("", port), DirectoryHandler)
		httpd.allow_reuse_address = True # faster to restart on same port
		ifs = all_interfaces()
		ip, port = httpd.server_address

		print "Started, will die in %d minute(s)" % die_in
		print "Use the following links:"

		for i in ifs:
			print "on http://%s:%d/ (%s) "  % (format_ip(i[1]), port, i[0])

		print "--- access logs ---"

		httpd.serve_forever()

	except TimeIsUpException:
		exit(0)
	except Exception as e:
		print "Unexpected error:", sys.exc_info()[0], e
		exit(1)



def all_interfaces():
	max_possible = 128 # arbitrary. raise if needed.
	bytes = max_possible * 32
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	names = array.array('B', '\0' * bytes)
	outbytes = struct.unpack('iL', fcntl.ioctl(
		s.fileno(),
		0x8912, # SIOCGIFCONF
		struct.pack('iL', bytes, names.buffer_info()[0])
		))[0]
	namestr = names.tostring()
	lst = []
	for i in range(0, outbytes, 40):
		name = namestr[i:i+16].split('\0', 1)[0]
		ip = namestr[i+20:i+24]
		lst.append((name, ip))
	return lst


def format_ip(addr):
	return str(ord(addr[0])) + '.' + \
		str(ord(addr[1])) + '.' + \
		str(ord(addr[2])) + '.' + \
		str(ord(addr[3]))


class DirectoryHandler(SimpleHTTPRequestHandler):

	def zipdir(self, path, zip):
		for root, dirs, files in os.walk(path):
			for file in files:
				zip.write(os.path.join(root, file))


	def sizeof_fmt(self, num):
		if num == '-':
			return '-'

		for x in ['B','KB','MB','GB']:
			if num < 1024.0:
				return "{:.2f} {}".format(num, x)
			num /= 1024.0
		return "{:.2f} {}".format(num, 'TB')


	def sort_headers(self, path, sort_column, sort_asc):
		str = '<tr>'
		for column in ['name', 'size', 'date']:
			if column == sort_column:
				possible_sort = 'desc' if sort_asc else 'asc'
			else:
				possible_sort = 'asc'

			str += '<th>'
			str += '<a href="{}?column={}&sort={}">{}</a>'.format(path, column, possible_sort, column)
			str += '</th>'

		str += '</tr>'
		return str


	def wrap_html(self, path, message, content):
		return '''
<!DOCTYPE html>
<html>
<head>
	<title>Directory listing for {}</title>
	<style>
body {{
	background-color: #333;
	color: #ddd;
}}
a {{
	color: #ddd;
}}
table {{
	width: 100%%;
}}
td {{
	padding: 0 10px 0 0;
}}
th {{
	text-align: left;
}}
	</style>
</head>
<body>
	<h2>Directory listing for {}</h2>
	<div class="message">{}</div>
	<hr>
	{}

	<hr>
	<form ENCTYPE="multipart/form-data" method="post">
		<input name="file" type="file"/>
		<input type="submit" value="upload"/>
	</form>
	<hr>
</body>
</html>
'''.format(path, path, message, content)

	def template_zip_link(self, path):
		return '<hr><a href="{}?zip">Download current folder as zip</a>'.format(path)

	def template_table(self, content):
		return "<table>{}</table>".format(content)

	def template_line(self, display_name, link, size, date):
		return '''<tr>
	<td class="link"><a href="{}">{}</a></td>
	<td class="size">{}</td>
	<td class="last-modified">{}</td>
</tr>'''.format(link, display_name, size, date)

	def template_up(self, path):
		if path != '/':
			parent_path = "/".join(path.split('/')[:-2])
			if parent_path == '': parent_path = '/'

			return '<tr><td colspan="3"><a href="{}">Up</a></td></tr>'.format(parent_path)
		return ""

	def add_lines(self, items):
		str = ""
		for item in items:
			str += self.template_line(cgi.escape(item['name']), urllib.quote(item['link']), self.sizeof_fmt(item['size']), item['date'])

		return str

	def sort(self, items, column, ascending):
		# list.sort(key=lambda a: a.lower())

		return sorted(items, key=lambda item: item[column], reverse=not ascending)

	def prepare_list(self, dir_path, names):
		list = []

		for name in names:
			fullname = os.path.join(dir_path, name)
			size = os.path.getsize(fullname)
			date_modified = datetime.datetime.fromtimestamp(os.path.getmtime(fullname)).strftime("%Y.%m.%d %R:%M")

			display_name = link = name

			if os.path.isdir(fullname):
				display_name = name + "/"
				link = name + "/"
				size = "-"
				if os.path.islink(fullname):
					display_name = name + "@"
					# Note: a link to a directory displays with @ and links with /

			list.append({
				'name': display_name,
				'link': link,
				'size': size,
				'date': date_modified})

		return list

	'''
	filter out the XXX?zip request
	'''
	def do_GET(self):
		self.message = 'Hello'
		if not self.server.should_respond:
			self.send_error(410)
			return ""

		if not self.path.endswith("?zip"):
			return SimpleHTTPRequestHandler.do_GET(self)

		# test if requested file is a directory
		if not os.path.isdir(curdir + self.path.split('?')[0]):
			return SimpleHTTPRequestHandler.do_GET(self)

		dirname = curdir + self.path.split('?')[0]
		filename = dirname.split(sep)[-2]

		if filename == '.': filename = 'root'

		zipfilename = '/tmp/' + filename + '.zip'

		zf = zipfile.ZipFile(zipfilename, 'w')
		self.zipdir(dirname, zf)
		zf.close()

		f = open(zipfilename)
		self.send_response(200)
		self.send_header('Content-type', 'application/zip')
		self.send_header("content-disposition", "inline; filename={}.zip".format(filename));
		self.end_headers()
		self.wfile.write(f.read())
		f.close()

	def do_POST(self):
		success, message = self.real_do_POST()

		self.message = message

		return SimpleHTTPRequestHandler.do_GET(self)


	def real_do_POST(self):

		form = cgi.FieldStorage(
			fp=self.rfile,
			headers=self.headers,
			environ={
				'REQUEST_METHOD': 'POST',
				'CONTENT-TYPE': self.headers['Content-Type']
				})

		filename = form['file'].filename
		data = form['file'].file.read()
		path = self.translate_path(self.path)
		fn = os.path.join(path, filename)
		try:
			out = open(fn, 'wb')
		except IOError:
			return (False, "Can't create file to write, do you have permission to write?")

		out.write(data)

		# self.send_response(303, "/#success")



		return (True, "File '%s' upload success!" % fn)

	def list_directory(self, path):
		try:
			list = os.listdir(path)
		except os.error:
			self.send_error(403, "No permission to list directory")
			return None


		f = StringIO()

		# displaypath = cgi.escape(urllib.unquote(self.path))

		url_split = urlparse.urlsplit(self.path)
		displaypath = url_split[2]

		qs_split = cgi.parse_qsl(url_split[3].strip('/'))

		sort_asc = True
		sort_column = "name"

		for key, value in qs_split:
			if key == "column":
				sort_column = value
			elif key == "sort":
				sort_asc = value == "asc"

		items = self.prepare_list(path, list)

		sorted_items = self.sort(items, sort_column, sort_asc)

		table_content = self.sort_headers(displaypath, sort_column, sort_asc)
		table_content += self.template_up(displaypath)
		table_content += self.add_lines(sorted_items)

		out = self.template_table(table_content)
		out += self.template_zip_link(displaypath)

		f.write(self.wrap_html(displaypath, self.message, out))

		length = f.tell()
		f.seek(0)
		self.send_response(200)
		encoding = sys.getfilesystemencoding()
		self.send_header("Content-type", "text/html; charset={}".format(encoding))
		self.send_header("Content-Length", str(length))
		self.end_headers()
		return f


class StoppableHttpServer(SocketServer.TCPServer):

	def verify_request(self, request, client_address):
		global die_at

		now = datetime.datetime.now()

		too_late = now > die_at

		self.should_respond = not too_late

		return SocketServer.TCPServer.verify_request(self, request, client_address)


	def close_request(self, request):
		if not self.should_respond:
			# self.shutdown()
			raise TimeIsUpException("time's up error")
		else:
			return SocketServer.TCPServer.close_request(self, request)


class TimeIsUpException(Exception):
	"""Specific Exception to quit server"""
	def __init__(self, str):
		super(TimeIsUpException, self).__init__()
		self.str = str



if __name__ == '__main__':
	main(sys.argv[1:])

