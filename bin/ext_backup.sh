#!/bin/bash

#DIRECTORIES_TO_BACKUP="Dropbox src tools"
#TARGET="/media/$USER/my_backup"


while getopts 't:' flag; do
    case "${flag}" in
        t) TARGET=${OPTARG} ;;
        *) error "Unexpected option ${flag}" ;;
    esac
done


if [[ ! -x $TARGET ]]; then
    echo "No valid directory to backup on \"$TARGET\" was given"
    exit 1
fi

shift $(($OPTIND - 1))

DIRECTORIES_TO_BACKUP=$@

echo "backup of \"$DIRECTORIES_TO_BACKUP\" on \"$TARGET\""

# one by one
for directory in $DIRECTORIES_TO_BACKUP; do
    if [[ -x $directory ]]; then
        ibackup.sh $directory $TARGET/$directory
    fi
done


# if [[ -x /media/$USER/my_backup ]]; then
#     rsync -av $DIRECTORIES_TO_BACKUP $TARGET
# fi
