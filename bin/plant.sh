#!/bin/bash

PLANT_JAR=~/tools/plantuml.jar
JAVA_EXEC=java
VIEWER_EXEC=eog

function usage () {
  echo "
Usage:
  plant.sh <filename> <extension>
" 
  exit 1  
}

echo $@


if [[ -x $JAVA_EXEC ]]; then
 usage;
fi

if [[ -x $VIEWER_EXEC ]]; then
  usage;
fi

if [[ -x $PLANT_JAR ]]; then
  usage;
fi

if [[ $# < 2 ]]; then
  usage;
fi

TMP=/tmp/plant.sh
FILE=`basename $1 .$2`

mkdir -p $TMP

$JAVA_EXEC -jar $PLANT_JAR -o $TMP $1 && $VIEWER_EXEC $TMP/$FILE.png
