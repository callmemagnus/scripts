#!/usr/bin/env python
# taken from http://www.doughellmann.com/PyMOTW/smtpd/
import smtpd
import asyncore
import sys
import smtplib

host = 'localhost'
port = 10025

final_host = 'mail.magooweb.com'
final_port = 25
# uncomment to hard code the destination email # final_email = 'magnus.anderssen@magooweb.com'

if len(sys.argv) > 1:
    final_email = sys.argv[1]

try:
    final_email
except:
    print "Missing final email, configure it in the script or pass it as an argument to this script"
    exit(1)


print "Starting local SMTP server on", host, "on port", port

def send_to_final(mailfrom, rcpttos, data):
    # Prepare actual message
    message = """\
    From: %s
    To: %s

    original recipients : %s

    %s
    """ % (mailfrom, final_email, ", ".join(rcpttos), data)

    server = smtplib.SMTP(final_host)
    server.sendmail(mailfrom, [final_email], data)
    server.quit()


class CustomSMTPServer(smtpd.SMTPServer):

    def process_message(self, peer, mailfrom, rcpttos, data):
        # append to file

        print 'From:', mailfrom, "To:", rcpttos, "Length:", len(data), 'sent to:', final_email
        # print '--- Message START:'
        # print data
        # print '--- Message END\n\n'

        send_to_final(mailfrom, rcpttos, data)

        return


server = CustomSMTPServer((host, port), None)

asyncore.loop()
