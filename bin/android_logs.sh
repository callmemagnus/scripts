#!/bin/bash
#
# adb must be properly setup
#
# cyanogen stuff

function usage() {
    echo "Usage: `basename $0` [<target directory>]"
    if [[ "$1" != "" ]]; then
        echo -e "\n -> $1\n"
    fi
    exit
}

DATE=`date +%Y%m%d%H%M`

TARGET_DIR=/tmp
if [[ "$1" != "" ]]; then
    TARGET_DIR=$1
fi

if [[ ! -d $TARGET_DIR ]]; then
    usage "Not a directory: $TARGET_DIR"
fi

if [[ `adb get-state` != 'device' ]]; then
    usage "Device is not connected: `adb get-state`"
fi


# kernel log
echo -n "Kernel log... "
adb shell su -c dmesg > $TARGET_DIR/dmesg.${DATE}.log
echo "done"

# last kmesg
echo -n "kmesg... "
adb shell su -c "cat /proc/last_kmsg" > $TARGET_DIR/last_kmsg.${DATE}.log
echo "done"

# radio related
echo -n "Radio log... "
adb logcat -b radio -v time -d > $TARGET_DIR/logcat_radio.${DATE}.log
echo "done"

echo "You can find your logs here: $TARGET_DIR"

echo "Done"